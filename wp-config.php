<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'king_composer');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'bQ+S0GGv-i;1r]SFKA&?ML8Bb2WmBY?m@GQv&gdFtI-z25QOs@==vUs-wL-5X_at');
define('SECURE_AUTH_KEY',  'B$B$>?ZeSHt;%m>*eb<Ep~R9P?.R?:{=llGB*h0R!k,O5{-5^s>/X<F8jU1vIZH?');
define('LOGGED_IN_KEY',    'Va_qhEc37v_%!R :@F e}!-^^p8V;kFjd |TB4Hhn;/9=jLaWH;eF|])fy!K#CD0');
define('NONCE_KEY',        'Kr.|`4s=#,Z(2KyxHS!4y*pV/v0Gij^!rOb8^EtE!>{eg+J& >B@P6(tMNQt?x:u');
define('AUTH_SALT',        '`v&>v1J2|!:!cHau89O91g&IPp3t &z{+Wl}jVV3g)gQi[[w{lN%@-7]l/{a.;?]');
define('SECURE_AUTH_SALT', 'b.E5Q&x9`*>DM|_9nQcKwA|sA?gR]uk3R(YO)_Dwa-b9clFzf5w{)/}BSl)h**ml');
define('LOGGED_IN_SALT',   'ev2#.a#Ls*`J6zvs>>5pbBsp{@ESo;E!,<QIBZfQ#%!&ObG:A]i6rxukd^:v8KM6');
define('NONCE_SALT',       'd~/eG;##PpM2QA`z~TX<IaWKB(7=*-&#yJ&Z;6}4>TH-chDm+C[/`lq#I8W- /fd');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_king_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
